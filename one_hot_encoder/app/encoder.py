"""
This is a custom OneHotEncoderEstimator

"""

from pyspark.ml.feature import OneHotEncoderEstimator
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineEstimator


class CustomerOneHotEncoderEstimator(OneHotEncoderEstimator, \
                                    AbstractSparkPipelineEstimator):

    def __init__(self, name, xpresso_run_name, \
                    inputCols=None, outputCols=None, \
                    handleInvalid='error', dropLast=True):

        OneHotEncoderEstimator.__init__(self, inputCols=inputCols, \
                                            outputCols=outputCols, \
                                            handleInvalid=handleInvalid, \
                                            dropLast=dropLast)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.xpresso_run_name = xpresso_run_name
    
    def _fit(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        model = super()._fit(dataset)
        return model