__all__ = ["AbstractSparkPipelineEstimator", \
            "AbstractSparkPipelineTransformer", \
            "XprPipeline"]

__author__ = "xpresso.ai"

import os
import argparse

from pyspark.sql import SparkSession
from pyspark.ml.pipeline import Pipeline
from pyspark.ml.pipeline import PipelineModel
from pyspark.ml.base import Estimator, Transformer

from xpresso.ai.core.commons.exceptions.xpr_exceptions import XprExceptions
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    PipelineComponentException
from xpresso.ai.core.data.pipeline.report_status import StatusReporter


class AbstractSparkPipelineComponent(AbstractPipelineComponent):
    """
    Xpresso's abstract base class for pyspark base classes

    """

    def __init__(self, name=None):
        super().__init__(name=name)

    def start(self, xpresso_run_name):
        """
        start the experiment corresponding to the xpresso_run_name
        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        self.logger.info("Parent component starting")
        self.xpresso_run_name = xpresso_run_name

        if self.status_reporter is None:
            self.status_reporter = StatusReporter(component_name=self.name,
                                                  xpresso_run_name=self.xpresso_run_name)

        try:
            self.controller.pipeline_component_started(self.xpresso_run_name,
                                                       self.name)
        except XprExceptions as e:
            print("Warning: {}".format(e.message))

    def pipeline_completed(self):
        """
        Marks the completion of Xpresso component.
        Reports completion status to the xpresso controller.

        """
        self.logger.info("Parent pipeline completed")

    def completed(self, push_exp=False):
        """
        Marks the completion of Xpresso component.
        Reports completion status to the xpresso controller.

        """
        self.logger.info("Parent component completed")

    def check_run_status(self):
        print("Stopping the thread")


class AbstractSparkPipelineEstimator(AbstractSparkPipelineComponent):
    """
    Xpresso's abstract class for pyspark type `pyspark.ml.base.Estimator`
    Extend pysaprk Estimator with this class to avail xpresso.ai features

    For example the below created MyStringIndexer can be used instead of
    StringIndexer in XprPipeline.
    >>> from pyspark.ml.feature import StringIndexer
    >>> from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
        AbstractSparkPipelineEstimator

    >>> class MyStringIndexer(StringIndexer, AbstractSparkPipelineEstimator):

            def __init__(self, name,
                            xpresso_run_name,
                            inputCol=None,
                            outputCol=None,
                            handleInvalid='error',
                            stringOrderType='frequencyDesc'):
                class_name = self.__class__.__name__
                StringIndexer.__init__(self,
                                        inputCol=inputCol,
                                        outputCol=outputCol,
                                        handleInvalid=handleInvalid,
                                        stringOrderType=stringOrderType)
                AbstractSparkPipelineEstimator.__init__(self, name=name)
                self.xpresso_run_name = xpresso_run_name

    """

    def __init__(self, name=None):
        super().__init__(name=name)


class AbstractSparkPipelineTransformer(AbstractSparkPipelineComponent):
    """
    Xpresso's abstract class for pyspark type `pyspark.ml.base.Transformer`
    Extend pysaprk Transformer with this class to avail xpresso.ai features

    For example the below created MyVectorAssembler can be used instead of
    VectorAssembler in XprPipeline.
    >>> from pyspark.ml.feature import VectorAssembler
    >>> from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
        AbstractSparkPipelineTransformer

    >>> class MyVectorAssembler(VectorAssembler, AbstractSparkPipelineTransformer):
            def __init__(self, name, xpresso_run_name, inputCols=None, outputCol=None):
                class_name = self.__class__.__name__
                VectorAssembler.__init__(self, \
                                inputCols=inputCols, \
                                outputCol=outputCol)
                AbstractSparkPipelineTransformer.__init__(self, name=name)
                self.xpresso_run_name = xpresso_run_name
    """

    def __init__(self, name=None):
        super().__init__(name=name)


class XprPipeline(Pipeline, AbstractSparkPipelineComponent):
    """
    Xpresso's abstract class for pyspark type `from pyspark.ml.pipeline.Pipeline`

    For example:
    >>> from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component \
        import XprPipeline
    >>> import sys

    >>> class MyPipeline(XprPipeline):
            def __init__(self, sys_args=[]):
                XprPipeline.__init__(self, sys_args)
    >>> sys_args = sys.argv[1:] #ignore the first arg
    >>> pipeline = MyPipeline(sys_args=sys_args)

    """

    def __init__(self, sys_args):
        """
        __init__(self, sys_args)

        `sys_args` is the list of all command line arguments passed to the spark
        job using `run_parameters` when deploying `component` or starting pipeline 
        `experiment`

        """
        parser = argparse.ArgumentParser()

        parser.add_argument('--xpresso_run_name',
                            type=str,
                            default='xpresso_run_name-value',
                            help='xpresso_run_name help')
        args, unknown = parser.parse_known_args(sys_args)
        xpresso_run_name = args.xpresso_run_name

        if not xpresso_run_name:
            msg = f'`xpresso_run_name` cannot be empty!'
            print(msg)
            raise PipelineComponentException(msg)

        self.spark_session = SparkSession \
            .builder \
            .appName(xpresso_run_name) \
            .getOrCreate()

        Pipeline.__init__(self)
        AbstractSparkPipelineComponent.__init__(self, name=xpresso_run_name)

        self.name = os.environ["PROJECT_NAME"]
        self.xpresso_run_name = args.xpresso_run_name

        if self.status_reporter is None:
            self.status_reporter = StatusReporter(component_name=self.name,
                                                  xpresso_run_name=self.xpresso_run_name)

    def _fit(self, dataset):

        stages = self.getStages()
        for stage in stages:
            if not (isinstance(stage, Estimator) or isinstance(stage, Transformer)):
                raise TypeError(
                    "Cannot recognize a pipeline stage of type %s." % type(stage))

        indexOfLastEstimator = -1
        for i, stage in enumerate(stages):
            if isinstance(stage, Estimator):
                indexOfLastEstimator = i
        transformers = []
        for i, stage in enumerate(stages):
            if i <= indexOfLastEstimator:
                if isinstance(stage, Transformer):
                    transformers.append(stage)
                    dataset = stage.transform(dataset)
                else:  # must be an Estimator
                    model = stage.fit(dataset)
                    transformers.append(model)
                    if i < indexOfLastEstimator:
                        dataset = model.transform(dataset)
            else:
                transformers.append(stage)

            stage.state = dataset

            # report the status only when stage is 
            # xpresso's component as well.
            # to retain native pyspark compatibility
            if isinstance(stage, AbstractPipelineComponent):
                status = {'status':
                        {'status' : f'Component completed {stage.name}'}
                    }
                try:
                    stage.report_status(status)
                except Exception as e:
                    pass
            stage.completed()

        print(f'Now returning PipelineModel', flush=True)
        return PipelineModel(transformers)

    def save_model(self, model, path):
        """
        Used to the save `model` at `path` in hdfs
        Args:
            model (pyspark.ml.Model) : fitted ml model
            path  (str) : path to save the `model`
        """

        try:
            model.save(path)
            return True
        except Exception as e:
            print(str(e))
            return False

    def save_predictions(self, predictions, path):
        """
        Used to the save `predictions` at `path` in hdfs
        Args:
            predictions (pyspark.sql.DataFrame) : predictions dataframe
            path  (str) : path to save the `predictions`
        """
        try:
            predictions.write.format("csv").save(path)
            return True
        except Exception as e:
            print(str(e))
            return False
